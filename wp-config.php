<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3cHNYhhUAo(1K`UA/t<e<A@Li^F-9qriI0,C)u8YTEi,@|~GRPOh&K@urF|&_UCU' );
define( 'SECURE_AUTH_KEY',  '9S0q6 6/A4LU=q.Jec1MbP4NsDI~?@O*%wljShD=cvcs,~]t{I`3pxwwjdi &V#4' );
define( 'LOGGED_IN_KEY',    '@nlff6-[-nch%ttm;{cSy@yB^z]S^R~r@G%dnAXV{-,V2Ihu& PY~>ZA?%!Fy6bm' );
define( 'NONCE_KEY',        'dih9:&p&#6z~L`/-JI/S|(Sz{wG=BwR=P|:MV~8V4ISr,}vOYR~ z(8->c9NKSLv' );
define( 'AUTH_SALT',        'Z,JzM&ye#>p*MgaC$lx13,/3W,mNm]t-Qj!Te,kJV6<*B&-4x7g1AcY6}Tc]9+49' );
define( 'SECURE_AUTH_SALT', '?tHQY~>e@/4qi=zkC`t7s=FSK7vq6<MDRy~3K=go7zNdtoGaGyM(P2]866 /5^/M' );
define( 'LOGGED_IN_SALT',   ',!}#S7fE-OPfDq>V%R,S;<D#q!3cF2XD2uHC%?[Eq3M/E?blzy~%WB`~+f|o8wAk' );
define( 'NONCE_SALT',       'U(E ov836BPh<#NP}j*x)}TYo)-U[t$%8~jpj#`BJa_+;9Xa#{twcZQw-GZI@s}]' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
